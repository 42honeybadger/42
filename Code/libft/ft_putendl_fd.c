/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mconraux <mconraux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/19 14:57:15 by mconraux          #+#    #+#             */
/*   Updated: 2013/11/19 17:37:36 by mconraux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putendl_fd(char const *str, int fd)
{
	while (*str != '\0')
	{
		ft_putchar_fd(*str, fd);
		str++;
	}
	write(fd, "\n", 1);
}
