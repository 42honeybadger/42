/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mconraux <mconraux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/19 17:38:00 by mconraux          #+#    #+#             */
/*   Updated: 2013/11/19 18:43:23 by mconraux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>

int		ft_strlen(char *str);
void*	ft_memset(void *b, int c, size_t len);
char*	ft_itoa(int nbr);
void	ft_putchar(char c);
void	ft_putstr(char const *str);
void	ft_putendl(char const* str);
void	ft_putnbr(int nbr);
void	ft_putchar_fd(char c, int fd);
void	ft_putstr_fd(char const *str, int fd);
void	ft_putendl_fd(char const* str, int fd);
void	ft_putnbr_fd(int nbr, int fd);

