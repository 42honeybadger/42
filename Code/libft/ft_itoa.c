/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mconraux <mconraux@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/19 15:18:54 by mconraux          #+#    #+#             */
/*   Updated: 2013/11/19 18:06:09 by mconraux         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_nbrlen(int nbr)
{
	int i;

	i = 1;

	while (nbr > 10)
	{
		nbr = nbr / 10;
		i++;
	}
	return(i);
}

char* ft_write_itoa(char* str, int nbr)
{
	if (nbr < 10)
		*str = nbr + '0';
	else
	{
		ft_write_itoa(str - 1, nbr / 10);
		ft_write_itoa(str - 1, nbr % 10);
	}
	return(str);
}

char*	ft_itoa(int nbr)
{
	char *str;
	int index;
	int len;

	index = 0;
	len = ft_nbrlen(nbr);

	str = malloc(sizeof(char) * (ft_nbrlen(nbr) + 1));
	return(ft_write_itoa(str + ft_nbrlen(nbr), nbr));
}
